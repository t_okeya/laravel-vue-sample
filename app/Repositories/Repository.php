<?php

namespace App\Repositories;

use Closure;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent;
use Illuminate\Support\Collection;

/**
 * Repository基底クラス.
 *
 * @package App\Repositories
 */
class Repository
{
    /** @var string $default_sort_query デフォルトソート. */
    protected $default_sort_query = '-id';
    /** @var string モデルクラス名. */
    protected $model;

    /**
     * Repository constructor.
     */
    public function __construct()
    {
        $class  = class_basename($this);
        $suffix = 'Repository';
        $len    = -strlen($suffix);

        if (substr($class, $len) === $suffix)
        {
            $class = substr($class, 0, $len);
        }
        $this->model = "\\App\\Models\\$class";
    }

    /**
     * @param array $attributes
     * @return Eloquent\Model
     */
    protected function instance($attributes = [])
    {
        return new $this->model($attributes);
    }

    /**
     * @return Eloquent\Builder
     */
    protected function getQuery()
    {
        return $this->instance()->newQuery();
    }

    /**
     * @param $id
     * @return Eloquent\Builder|Eloquent\Builder[]|Eloquent\Collection|Eloquent\Model
     */
    public function findOrFail($id)
    {
        return $this->getQuery()->findOrFail($id);
    }

    /**
     * @param array $condition
     * @return LengthAwarePaginator
     */
    public function search($condition = [])
    {
        $condition = collect($condition);
        $query     = $this->getQuery();
        $this->applySortQuery($condition->get('sort'), $query);
        $this->applyCondition($condition, $query);
        return $query->paginate($condition->get('limit', config('application.const.list.max')));
    }

    /**
     * @param array $condition
     * @return Collection
     */
    public function findAll($condition = [])
    {
        $condition = collect($condition);
        $query     = $this->getQuery();
        $this->applySortQuery($condition->get('sort'), $query);
        $this->applyCondition($condition, $query);
        return $query->take($condition->get('limit', config('application.const.list.no_limit')))->get()->toBase();
    }

    /**
     * @param array $values
     * @return Eloquent\Builder|Eloquent\Builder[]|Eloquent\Collection|Eloquent\Model
     */
    public function create(array $values)
    {
        $model = $this->instance($values);
        $model->save();
        return $this->findOrFail($model->getKey());
    }

    /**
     * @param $id
     * @param array $values
     * @return Eloquent\Builder|Eloquent\Builder[]|Eloquent\Collection|Eloquent\Model
     */
    public function update($id, array $values)
    {
        $model = $this->getQuery()->findOrFail($id);
        $model->update($values);
        return $this->findOrFail($id);
    }

    /**
     * @param $ids
     * @param array $data
     * @return int
     */
    public function updateByIds($ids, array $data){
        return $this->getQuery()
            ->whereIn('id', $ids)
            ->update($data);
    }

    /**
     * @param $ids
     * @return array
     */
    public function destroy($ids)
    {
        $deleted_ids = [];
        $ids = is_array($ids) ? $ids : func_get_args();
        $key = $this->instance()->getKeyName();

        foreach ($this->getQuery()->whereIn($key, $ids)->get() as $model)
        {
            if ($model->delete())
            {
                $deleted_ids[] = $model->id;
            }
        }

        return $deleted_ids;
    }

    /**
     * @param $condition
     * @param null $query
     * @return Eloquent\Builder
     */
    protected function applyCondition($condition, $query = null)
    {
        ($condition);
        return $query ?: $this->getQuery();
    }

    /**
     * @param $sort
     * @param null $query
     * @return Eloquent\Builder
     */
    protected function applySortQuery($sort, $query = null)
    {
        $query = $query ?: $this->getQuery();

        if ($sort instanceof Closure)
        {
            call_user_func($sort, $query);
            return $query;
        }
        elseif ($sort === 'shuffle')
        {
            return $query->inRandomOrder();
        }
        else
        {
            list($default_key, $default_order) = $this->parseSortQuery($this->default_sort_query);
            list($sort_key, $sort_order) = $this->parseSortQuery($sort, $default_key, $default_order);
            return $query->orderBy($sort_key, $sort_order);
        }
    }

    /**
     * @param $sort_query
     * @param string $default_sort
     * @param string $default_order
     * @return array|string[]
     */
    protected function parseSortQuery($sort_query, $default_sort = '', $default_order = 'asc')
    {
        $sort_query = (string)$sort_query;
        if ($sort_query !== '')
        {
            if (strncmp($sort_query, '-', 1) === 0)
            {
                $order = 'desc';
                $sort_query = substr($sort_query, 1);
            }
            else
            {
                $order = 'asc';
            }

            if ($sort_query !== '')
            {
                return [
                    $sort_query,
                    $order,
                ];
            }
        }
        return [
            $default_sort,
            $default_order,
        ];
    }

}
