# Installation

To use the package, register the service provider in `config/app.php`.

```
'providers' => [
    // ...
    Custom\Stripe\Providers\StripeServiceProvider::class,
],

'aliases' => [
    // ...
    'CustomStripe' => Custom\Stripe\Facades\Stripe::class,
]
```


# Configuration

To configure your connection settings, execute the following command.

```sh
php artisan vendor:publish --provider="Custom\Stripe\Providers\StripeServiceProvider"
```

Then set the following environment variables in `.env` file.

```
STRIPE_KEY
STRIPE_SECRET
TIMEOUT
CONNECT_TIMEOUT
PROXY_HOST
PROXY_PORT
```


# Features

```

```

have only one thing one can do "reference-related".


# Usage

## Create the Instance

To use the API First of all you have to create the API instance.

### Example

```
use CustomStripe;
```

```
public function __construct()
{
    $this->customer = CustomStripe::driver('Customer');
}
```

## Get a customer.

### Example

```
$results = $this->customer->retrieve($customer_id);
```
