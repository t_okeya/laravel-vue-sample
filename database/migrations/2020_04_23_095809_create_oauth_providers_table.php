<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOauthProvidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oauth_providers', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('ID');
            $table->bigInteger('user_id')->unsigned()->comment('ユーザID');
            $table->string('provider')->comment('プロバイダ名');
            $table->string('provider_user_id')->index()->comment('プロバイダユーザID');
            $table->string('access_token')->nullable()->comment('アクセストークン');
            $table->string('refresh_token')->nullable()->comment('リフレッシュトークン');
            $table->timestamps();
            $table->engine = "InnoDB Comment 'ソーシャル認証'";

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oauth_providers');
    }
}
