<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/**
 * factory(App\Models\User::class, 1)->create()
 */
$factory->define(User::class, function (Faker $faker) {

    $email    = $faker->unique()->safeEmail;
    $password = 'password';
    $token    = Str::random(10);

    return [
        'name'              => $faker->name,
        'email'             => $email,
        'email_verified_at' => now(),
        'password'          => bcrypt($password),
        'remember_token'    => $token,
        'plan_id'           => factory(App\Models\Plan::class)->create()->get('id'),
        'customer_id'       => 'cus_' . Str::random(14)
    ];
});
