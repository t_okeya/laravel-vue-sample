<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Class RepositoryServiceProvider.
 *
 * @package App\Providers
 */
class RepositoryServiceProvider extends ServiceProvider
{
    /** @var string $interface インターフェースのパス. */
    protected $interface = 'App\Repositories\Contracts';
    /** @var string $implement 実処理のパス. */
    protected $implement = 'App\Repositories';

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton("{$this->interface}\UserRepository", "{$this->implement}\UserRepository");
        $this->app->singleton("{$this->interface}\PaymentSpriteRepository", "{$this->implement}\PaymentSpriteRepository");
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

    }
}
