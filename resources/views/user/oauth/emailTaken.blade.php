@extends('user.errors.layout')

@section('title', 'Login Error')

@section('message', 'Email already taken.')
