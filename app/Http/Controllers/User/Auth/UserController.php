<?php

namespace App\Http\Controllers\User\Auth;

use App\Http\Controllers\User\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class UserController.
 *
 * @package App\Http\Controllers\User\Auth
 */
class UserController extends Controller
{
    /**
     * Get authenticated user.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function current(Request $request)
    {
        return response()->json($request->user());
    }
}
