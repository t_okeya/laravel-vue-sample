<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Class EmailTakenException.
 *
 * @package App\Exceptions
 */
class EmailTakenException extends Exception
{
    /**
     * Render the exception as an HTTP response.
     *
     * @param Request $request
     * @return Response
     */
    public function render($request)
    {
        ($request);
        return response()->view('user.oauth.emailTaken', [], 400);
    }
}
