<?php

namespace App\Http\Controllers\User\Settings;

use App\Http\Controllers\User\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

/**
 * Class ProfileController.
 *
 * @package App\Http\Controllers\User\Settings
 */
class ProfileController extends Controller
{
    /**
     * Update the user's profile information.
     *
     * @param Request $request
     * @return mixed
     * @throws ValidationException
     */
    public function update(Request $request)
    {
        $user = $request->user();

        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$user->id,
        ]);

        return tap($user)->update($request->only('name', 'email'));
    }
}
