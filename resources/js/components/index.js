import Vue from 'vue'
import Button from './Button'
import Card from './Card'
import Checkbox from './Checkbox'
import Child from './Child'
import Modal from './Modal'
import { HasError, AlertError, AlertSuccess } from 'vform'

// Components that are registered globaly.
[
  Button,
  Card,
  Checkbox,
  Child,
  Modal,
  HasError,
  AlertError,
  AlertSuccess
].forEach(Component => {
  Vue.component(Component.name, Component)
})
