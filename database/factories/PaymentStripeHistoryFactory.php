<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\PaymentStripeHistory;
use Faker\Generator as Faker;

/**
 * factory(App\Models\PaymentStripeHistory::class, 1)->create()
 */
$factory->define(PaymentStripeHistory::class, function (Faker $faker) {
    return [
        'user_id'           => factory(App\Models\User::class)->create()->get('id'),
        'payment_method_id' => factory(App\Models\PaymentMethod::class)->create()->get('id'),
        'requests'          => $faker->text(),
        'results'           => $faker->text(),
    ];
});
