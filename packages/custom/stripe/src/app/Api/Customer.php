<?php

namespace Custom\Stripe\Api;

use Custom\Stripe\Results\CardResult;
use Custom\Stripe\Results\CustomerResult;
use Stripe\HttpClient\CurlClient;
use Stripe\Customer as StripeCustomer;

/**
 * Stripe\Customer API ラッピングしたクラス.
 *
 * @package Custom\Stripe\Api
 */
class Customer extends Base
{

    /**
     * コンストラクタです.
     *
     * @param CurlClient|null $client
     * @param array $options
     */
    public function __construct(CurlClient $client = null, array $options = [])
    {
        parent::__construct($client, $options);
    }

    /**
     * 顧客情報取得.
     *
     * @param string $customer_id
     * @return CustomerResult
     * @throws \Stripe\Exception\ApiErrorException
     */
    public function retrieve(string $customer_id)
    {
        $response = StripeCustomer::retrieve($customer_id);
        return new CustomerResult($response->toArray());
    }

    /**
     * 顧客情報作成.
     *
     * @param string $user_id
     * @param string $user_name
     * @param string $user_email
     * @param string $sprite_token
     * @return CustomerResult
     * @throws \Stripe\Exception\ApiErrorException
     */
    public function create(string $user_id, string $user_name, string $user_email, string $sprite_token)
    {
        $response = StripeCustomer::create([
            'name'        => $user_name,
            'email'       => $user_email,
            'description' => 'USER_ID:' . $user_id,
            'source'      => $sprite_token // デフォルト決済情報となる. 追加したい場合は、カード作成APIを使用
        ]);
        return new CustomerResult($response->toArray());
    }

    /**
     * カード情報全取得.
     *
     * @param string $customer_id
     * @param int $limit
     * @return CardResult[]
     * @throws \Stripe\Exception\ApiErrorException
     */
    public function allSource(string $customer_id, int $limit = 10)
    {
        $results = [];

        $responses = StripeCustomer::allSources($customer_id, ['object' => 'card', 'limit' => $limit]);

        foreach ($responses as $key => $response)
        {
            array_push($results, new CardResult($response->toArray()));
        }

        return $results;
    }

    /**
     * カード情報取得.
     *
     * @param string $customer_id
     * @param string $card_id
     * @return CardResult
     * @throws \Stripe\Exception\ApiErrorException
     */
    public function retrieveSource(string $customer_id, string $card_id)
    {
        $response = StripeCustomer::retrieveSource($customer_id, $card_id);
        return new CardResult($response->toArray());
    }

    /**
     * カード情報更新.
     *
     * @param string $customer_id
     * @param string $token
     * @return CardResult
     * @throws \Stripe\Exception\ApiErrorException
     */
    public function createSource(string $customer_id, string $token)
    {
        $response = StripeCustomer::createSource($customer_id, ['source' => $token]);
        return new CardResult($response->toArray());
    }

    /**
     * カード情報削除.
     *
     * @param string $customer_id
     * @param string $card_id
     * @return CardResult
     * @throws \Stripe\Exception\ApiErrorException
     */
    public function deleteSource(string $customer_id, string $card_id)
    {
        $response = StripeCustomer::deleteSource($customer_id, $card_id);
        return new CardResult($response->toArray());
    }
}
