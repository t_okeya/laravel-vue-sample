<?php

// 元のconfig/
return [

    'model'           => App\Models\User::class, // config/service.php stripe.model に config('custom.stripe.model') を設定
    'key'             => env('STRIPE_KEY'),      // config/service.php stripe.key に config('custom.stripe.key') を設定
    'secret'          => env('STRIPE_SECRET'),   // config/service.php stripe.secret に config('custom.stripe.secret') を設定
    'timeout'         => env('TIMEOUT', 80),         // デフォルト80秒
    'connect_timeout' => env('CONNECT_TIMEOUT', 30), // デフォルト30秒
    'proxy_host'      => env('PROXY_HOST', 'proxy.local'),
    'proxy_port'      => env('PROXY_PORT', -1),

];
