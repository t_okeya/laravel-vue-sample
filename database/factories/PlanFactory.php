<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Plan;
use Faker\Generator as Faker;

/**
 * factory(App\Models\Plan::class, 1)->create()
 */
$factory->define(Plan::class, function (Faker $faker) {
    return [
        'name'         => $faker->name,
        'unit_price'   => $faker->numberBetween(500, 1000),
        'tax_category' => $faker->numberBetween(1, 3),
    ];
});
