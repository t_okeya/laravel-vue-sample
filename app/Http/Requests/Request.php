<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Collection;

/**
 * Class Request.
 *
 * @package App\Http\Requests
 */
class Request extends FormRequest
{
    /** @var array|Collection */
    protected $_fields;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    protected function fields()
    {
        return [];
    }

    /**
     * @param $name
     * @param string $label
     * @param string $rule
     * @return array
     */
    public function field($name, $label = '', $rule = '')
    {
        return compact('name', 'label', 'rule');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->toCollect();

        return $this->_fields->pluck('rule', 'name')->toArray();
    }

    /**
     * @return array
     */
    public function attributes()
    {
        $this->toCollect();

        return $this->_fields->pluck('label', 'name')->toArray();
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [];
    }

    /**
     * @return array
     */
    public function values()
    {
        $this->toCollect();
        $only = $this->_fields->pluck('name')->unique()->toArray();
        return $this->only($only);
    }

    /**
     * To Collection
     */
    private function toCollect()
    {
        if (is_null($this->_fields)) {
            $this->_fields = collect($this->fields());
        }
    }

}
