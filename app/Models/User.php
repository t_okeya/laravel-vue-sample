<?php

namespace App\Models;

use App\Notifications\User\VerifyEmail;
use App\Notifications\User\ResetPassword;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent;
use Illuminate\Database\Eloquent\Relations;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * Class User.
 *
 * @package App\Models
 *
 * @property mixed id
 * @property mixed name
 * @property mixed email
 * @property mixed email_verified_at
 * @property mixed password
 * @property mixed remember_token
 * @property mixed plan_id
 * @property mixed customer_id
 * @property Carbon created_at
 * @property Carbon updated_at
 * @property Carbon deleted_at
 * @property Relations\HasOne plan
 * @property Relations\HasMany oauth_providers
 * @property Relations\HasMany payment_methods
 * @property Eloquent\Builder|Eloquent\Model|object|null default_payment_method
 */
class User extends Authenticatable implements JWTSubject, MustVerifyEmail
{
    use Notifiable, Eloquent\SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'plan_id', 'customer_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'default_payment_method'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    // Override CanResetPassword

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }

    // Override MustVerifyEmail

    /**
     * Send the email verification notification.
     *
     * @return void
     */
    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmail);
    }

    // Override JWTSubject

    /**
     * @return int
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    // Relation

    /**
     * @return Relations\HasOne
     */
    public function plan()
    {
        return $this->hasOne(Plan::class);
    }

    /**
     * @return Relations\HasMany
     */
    public function oauth_providers()
    {
        return $this->hasMany(OauthProvider::class);
    }

    /**
     * @return Relations\HasMany
     */
    public function payment_methods()
    {
        return $this->hasMany(PaymentMethod::class);
    }

    // Attribute

    /**
     * デフォルト決済情報を返却します.
     *
     * @return Eloquent\Builder|Eloquent\Model|object|null
     */
    public function getDefaultPaymentMethodAttribute()
    {
        $query = $this->payment_methods()->getQuery();
        return $query->where('default_used', true)->limit(1)->first();
    }
}
