<?php

namespace Custom\Stripe\Api;

use Custom\Stripe\Results\ChargeResult;
use Stripe\HttpClient\CurlClient;
use Stripe\Charge as StripeCharge;

/**
 * Stripe\Charge API ラッピングしたクラス.
 *
 * @package Custom\Stripe\Api
 */
class Charge extends Base
{
    /**
     * コンストラクタです.
     *
     * @param CurlClient|null $client
     * @param array $options
     */
    public function __construct(CurlClient $client = null, array $options = [])
    {
        parent::__construct($client, $options);
    }

    /**
     * 請求情報作成.
     *
     * @param string $customer_id Stripe側の顧客ID
     * @param string $card_id Stripe側のカードID
     * @param int $amount 請求金額
     * @param string $currency 通貨 { デフォルト:jpy }
     * @return ChargeResult
     * @throws \Stripe\Exception\ApiErrorException
     */
    public function create(string $customer_id, string $card_id, int $amount, string $currency = 'jpy')
    {
        $response = StripeCharge::create([
            'amount'   => $amount,
            'currency' => $currency,
            'customer' => $customer_id,
            'source'   => $card_id,
        ]);
        return new ChargeResult($response->toArray());
    }
}
