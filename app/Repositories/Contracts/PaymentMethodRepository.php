<?php

namespace App\Repositories\Contracts;

use App\Models\PaymentMethod;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent;
use Illuminate\Support\Collection;

/**
 * Interface PaymentMethodRepository.
 *
 * @package App\Repositories\Contracts
 */
interface PaymentMethodRepository
{
    /**
     * IDを条件にデータを1件取得します.
     *
     * @param mixed $id
     * @return Eloquent\Collection|PaymentMethod
     */
    public function findOrFail($id);

    /**
     * 検索します.
     *
     * @param array $condition 検索条件
     * @return LengthAwarePaginator
     */
    public function search($condition);

    /**
     * 条件に一致するデータをすべて取得します.
     *
     * @param array $condition 検索条件
     * @return Collection
     */
    public function findAll($condition);

    /**
     * 新規作成します.
     *
     * @param array $values key-value配列
     * @return Eloquent\Collection|PaymentMethod
     */
    public function create(array $values);

    /**
     * IDを条件に更新します.
     *
     * @param mixed $id
     * @param array $values key-value配列
     * @return Eloquent\Collection|PaymentMethod
     */
    public function update($id, array $values);

    /**
     * IDの配列を条件に更新をします.
     *
     * @param $ids
     * @param array $data
     * @return mixed
     */
    public function updateByIds($ids, array $data);

    /**
     * IDの配列を条件に削除します.
     *
     * @param array|int|string $ids
     * @return int[]|string[] 削除したIDリスト
     */
    public function destroy($ids);
}
