<?php

namespace Custom\Stripe\Results;

// サンプル
// {
//  "id": "card_1Ggnb7FFcV8mRKd2IKUxO5Rc",
//  "object": "card",
//  "address_city": null,
//  "address_country": null,
//  "address_line1": null,
//  "address_line1_check": null,
//  "address_line2": null,
//  "address_state": null,
//  "address_zip": null,
//  "address_zip_check": null,
//  "brand": "Visa",
//  "country": "US",
//  "customer": "cus_HFIBmyXADlZI2R",
//  "cvc_check": "pass",
//  "dynamic_last4": null,
//  "exp_month": 8,
//  "exp_year": 2021,
//  "fingerprint": "w4Kgg1jA7zBnCii0",
//  "funding": "credit",
//  "last4": "4242",
//  "metadata": {},
//  "name": null,
//  "tokenization_method": null
//}

/**
 * Stripe/Card オブジェクト.
 * https://stripe.com/docs/api/cards/object
 *
 * @package Custom\Stripe\Results
 *
 * @property string id "card_[ユニークID]"
 * @property string object "card"固定
 * @property string|null address_city 市/区/郊外/町/村
 * @property string|null address_country 国
 * @property string|null address_line1 住所1（住所/私書箱/会社名）
 * @property string|null address_line1_check [address_line1]が指定された場合のチェック結果：
 * @property string|null address_line2 住所2（アパート/スイート/ユニット/ビル）
 * @property string|null address_state 州/郡/県/地域
 * @property string|null address_zip 郵便番号
 * @property string|null address_zip_check [address_zip]が指定されている場合のチェック結果 { pass:合格, fail:失敗, unavailable:使用不可, unchecked:未チェック }
 * @property string brand ブランド名
 * @property string country カードの国を表す2文字のISOコード
 * @property string|null customer カードが属する顧客情報
 * @property string cvc_check CVCが提供されている場合のチェック結果 { pass:合格, fail:失敗, unavailable:使用不可, unchecked:未チェック }
 * @property string|null dynamic_last4 トークン化された番号の場合のみで、デバイスアカウント番号下4桁
 * @property int exp_month カード有効期限(月)を表す2桁の数字
 * @property int exp_year カード有効期限(年)を表す4桁の数字
 * @property string fingerprint カード番号を一意に識別するための情報
 * @property string funding カードの資金調達タイプ { credit:クレジット, debit:デビット, prepaid:プリペイド, unknown:不明 }
 * @property string last4 カードの下4桁
 * @property array metadata 付加情報
 * @property string|null name カード名義人
 * @property array available_payout_methods 利用可能な支払い方法のセット { [standard] or [standard, instant] }
 * @property string recipient カードが属する受信者情報
 * @property string|null tokenization_method カード番号がトークン化されている場合に使用 { amex_express_checkout, android_pay(Google Pay含む), apple_pay, masterpass, visa_checkout }
 * @property boolean deleted 削除されたかどうか(削除API実施時のみ)
 */
class CardResult extends Result
{
    /**
     * コンストラクタです.
     *
     * @param array $response
     */
    public function __construct(array $response = [])
    {
        parent::__construct($response);
    }

}
