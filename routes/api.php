<?php

use \Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'User'], function () {

    Route::group(['middleware' => 'guest:users'], function () {
        Route::post('login', 'Auth\LoginController@login');
        Route::post('register', 'Auth\RegisterController@register');
        Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
        Route::post('password/reset', 'Auth\ResetPasswordController@reset');
        Route::post('email/verify/{user}', 'Auth\VerificationController@verify')->name('verification.verify');
        Route::post('email/resend', 'Auth\VerificationController@resend');
        Route::post('oauth/{driver}', 'Auth\OAuthController@redirect');
        Route::get('oauth/{driver}/callback', 'Auth\OAuthController@callback')->name('oauth.callback');
    });

    Route::group(['middleware' => 'auth:users'], function () {
        Route::post('logout', 'Auth\LoginController@logout');
        Route::get('me', 'Auth\UserController@current');
        Route::patch('settings/profile', 'Settings\ProfileController@update');
        Route::patch('settings/password', 'Settings\PasswordController@update');
        Route::get('settings/default', 'Settings\PaymentController@default');
        Route::get('settings/payments', 'Settings\PaymentController@allPayments');
        Route::patch('settings/payment', 'Settings\PaymentController@update');
        Route::delete('settings/payment', 'Settings\PaymentController@delete');
    });

});
