<?php

namespace App\Http\Controllers\User\Settings;

use App\Http\Controllers\User\Controller;
use App\Models\PaymentMethod;
use App\Models\User;
use CustomStripe;

/**
 * Class PaymentController.
 *
 * @package App\Http\Controllers\User\Settings
 *
 * @property \Custom\Stripe\Api\Customer customer
 * @property \Custom\Stripe\Api\Charge charge
 */
class PaymentController extends Controller
{

    /**
     * コンストラクタです.
     */
    public function __construct()
    {
        $this->customer = CustomStripe::driver('customer');
        $this->charge   = CustomStripe::driver('charge');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Stripe\Exception\ApiErrorException
     */
    public function default(\Illuminate\Http\Request $request)
    {
        /** @var User $user. */
        $user = auth()->user();

        if ($user->default_payment_method)
        {
            /** @var PaymentMethod $payment_method. */
            $payment_method = $user->default_payment_method;
            // 決済情報1件取得
            $results = $this->customer->retrieveSource($payment_method->customer_id, $payment_method->card_id);

            return response()->json($results);
        }
        return response()->json();
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Stripe\Exception\ApiErrorException
     */
    public function allPayments(\Illuminate\Http\Request $request)
    {
        /** @var User $user. */
        $user = auth()->user();

        if ($user->payment_methods)
        {
            // 決済情報全取得
            $results = $this->customer->allSource($user->customer_id);
            return response()->json($results);
        }
        return response()->json();
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @throws \Stripe\Exception\ApiErrorException
     */
    public function update(\Illuminate\Http\Request $request)
    {

        $values = $request->only('token');

        try
        {
            /** @var User $target. */
            $target = User::query()->find(auth()->id());

            if (is_null($target->customer_id))
            {
                // 新規登録処理

                // 顧客情報とカード情報を登録
                $customer = $this->customer->create($target->id,
                    $target->name, $target->email, $values['token']);
                $target->update(['customer_id' => $customer->id]);

                $target->payment_methods()->create([
                    'customer_id'  => $customer->id,
                    'card_id'      => $customer->default_source,
                    'default_used' => true
                ]);
            }
            else
            {
                // 更新処理（カード追加）

                $bool = is_null($target->default_payment_method);

                // カード情報を新規追加
                $card = $this->customer->createSource($target->customer_id, $values['token']);

                $target->payment_methods()->create([
                    'customer_id'  => $target->customer_id,
                    'card_id'      => $card->id,
                    'default_used' => $bool
                ]);

//                // 請求
//                $this->charge->create($target->customer_id, $card->id, 1000);
            }
        }
        catch (\Stripe\Exception\ApiErrorException $e)
        {
            // カード登録失敗時には現段階では一律で別の登録カードを入れていただくように促すメッセージで統一.
            // カードエラーの類としては以下があるとのこと
            // １、カードが決済に失敗しました
            // ２、セキュリティーコードが間違っています
            // ３、有効期限が間違っています
            // ４、処理中にエラーが発生しました
            return;
        }
        return;
    }

    public function delete(\Illuminate\Http\Request $request)
    {
        $values  = $request->only('card_id');

        try
        {
            /** @var User $target. */
            $target = User::query()->find(auth()->id());

            $results = $this->customer->deleteSource($target->customer_id, $values['card_id']);

            $target->payment_methods()
                ->where([
                    'customer_id' => $target->customer_id,
                    'card_id'     => $values['card_id']
                ])
                ->delete();
        }
        catch (\Stripe\Exception\ApiErrorException $e)
        {
            //
            return response()->json();
        }

        return response()->json();
    }
}
