<?php

use \Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| SPA Routes
|--------------------------------------------------------------------------
|
| Here is where you can register SPA routes for your frontend. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "spa" middleware group.
|
*/

Route::group(['namespace' => 'User'], function() {
    Route::get('{path}', 'IndexController')->where('path', '(.*)');
});
