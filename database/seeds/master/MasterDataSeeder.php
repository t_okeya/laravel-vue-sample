<?php

use Illuminate\Support\Facades\DB;

class MasterDataSeeder extends BaseSeeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $app = parent::createApplication();

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $this->createTaxes();
        $this->createPlans();

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
    }

    private function createTaxes()
    {
        $values = [
            [
                'rate'       => 0.10,
                'start_date' => '2019-10-01',
                'end_date'   => '9999-12-31',
            ]
        ];

        foreach ($values as $key => $value)
        {
            parent::createTax($value);
        }
    }

    private function createPlans()
    {
        $values = [
            [
                'name'         => '無料会員',
                'unit_price'   => 0, // 0円
                'tax_category' => 2, // 外税
            ],
            [
                'name'         => '有料会員',
                'unit_price'   => 500, // 500円
                'tax_category' => 2,   // 外税
            ]
        ];

        foreach ($values as $key => $value)
        {
            parent::createPlan($value);
        }
    }
}
