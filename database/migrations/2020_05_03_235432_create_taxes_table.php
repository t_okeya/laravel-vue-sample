<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTaxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('taxes', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('ID');
            $table->decimal('rate', 5, 3)->comment('消費税率');
            $table->date('start_date')->comment('適用開始日');
            $table->date('end_date')->comment('適用終了日');
            $table->timestamps();
            $table->engine = "InnoDB Comment '消費税'";
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('taxes');
    }
}
