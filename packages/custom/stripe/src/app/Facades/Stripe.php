<?php

namespace Custom\Stripe\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * ファサード.
 *
 * @package Custom\Stripe\Facades
 */
class Stripe extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'stripe';
    }
}
