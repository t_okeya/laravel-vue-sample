<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\OauthProvider;
use Faker\Generator as Faker;

/**
 * factory(App\Models\OauthProvider::class, 1)->create()
 */
$factory->define(OauthProvider::class, function (Faker $faker) {
    return [
        'user_id'          => factory(App\Models\User::class)->create()->get('id'),
        'provider'         => 'github',
        'provider_user_id' => $faker->randomNumber(),
        'access_token'     => $faker->randomNumber(),
        'refresh_token'    => $faker->randomNumber(),
    ];
});
