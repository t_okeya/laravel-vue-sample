// auth.js
export const LOGOUT = 'LOGOUT'
export const SAVE_TOKEN = 'SAVE_TOKEN'
export const FETCH_USER = 'FETCH_USER'
export const FETCH_USER_SUCCESS = 'FETCH_USER_SUCCESS'
export const FETCH_USER_FAILURE = 'FETCH_USER_FAILURE'
export const UPDATE_USER = 'UPDATE_USER'

export const FETCH_CARDS = 'FETCH_CARDS'
export const FETCH_CARDS_SUCCESS = 'FETCH_CARDS_SUCCESS'
export const FETCH_CARDS_FAILURE = 'FETCH_CARDS_FAILURE'
export const UPDATE_CARDS = 'UPDATE_CARDS'
export const DELETE_CARD = 'DELETE_CARD'

// lang.js
export const SET_LOCALE = 'SET_LOCALE'
