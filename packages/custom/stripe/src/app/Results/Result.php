<?php

namespace Custom\Stripe\Results;

/**
 * StripeAPIレスポンス情報格納オブジェクト基底クラス.
 *
 * @package Custom\Stripe\Results
 */
class Result
{

    /**
     * コンストラクタです.
     *
     * @param array $response
     */
    public function __construct(array $response = [])
    {
        $this->set_response($response);
    }

    /**
     * レスポンス情報をプロパティに設定します.
     *
     * @param array $response
     */
    private function set_response(array $response = [])
    {
        foreach ($response as $key => $value)
        {
            $this->set($key, $value);
        }
    }

    // マジックメソッド.

    /**
     * @param $key
     * @return mixed|null
     */
    function __get($key)
    {
        return $this->get($key);
    }

    /**
     * @param $key
     * @param $value
     */
    function __set($key, $value)
    {
        $this->set($key, $value);
    }

    /**
     * @param $key
     * @param null $default
     * @return mixed|null
     */
    private function get($key, $default = null)
    {
        if (property_exists($key, $this))
        {
            return $this->{$key};
        }
        return $default;
    }

    /**
     * @param $key
     * @param $value
     */
    private function set($key, $value)
    {
        $this->{$key} = $value;
    }

}
