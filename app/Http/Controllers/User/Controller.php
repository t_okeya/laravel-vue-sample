<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller as BaseController;
use Illuminate\Support\Facades\Route;
use \Illuminate\Contracts\Foundation\Application;
use \Illuminate\Contracts\View\Factory;
use \Illuminate\View\View;

class Controller extends BaseController
{
    /**
     * viewを返します.
     * 引数にnullを渡すと、routingからview名を推測します.
     *
     * @param array $data
     * @param null $view
     * @return Application|Factory|View
     */
    protected function view($data = [], $view = null)
    {
        if (is_null($view))
        {
            $current_route_action = strtolower(Route::currentRouteAction());

            if (count(explode('@', $current_route_action)) >= 2)
            {
                list($path, $action) = explode('@', $current_route_action);
            }
            else
            {
                $path   = $current_route_action;
                $action = '';
            }

            $path = preg_replace('/\\\\/', '.', preg_replace('/controller$/', '',
                    preg_replace('/app\\\\http\\\\controllers\\\\/', '', $path)));

            return view($path . '.' . $action, $data);
        }
        else
        {
            return view($view, $data);
        }
    }
}
