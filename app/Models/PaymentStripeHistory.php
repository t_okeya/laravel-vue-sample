<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations;

/**
 * Class PaymentStripeHistory.
 *
 * @package App\Models
 *
 * @property mixed user_id ユーザID
 * @property mixed payment_stripe_id StripeID
 * @property mixed Requests リクエスト情報
 * @property mixed Results レスポンス情報
 * @property Carbon created_at
 * @property Carbon updated_at
 * @property User|Collection|Relations\BelongsTo user
 * @property PaymentMethod|Collection|Relations\BelongsTo payment_stripe
 */
class PaymentStripeHistory extends Model
{
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'Requests'  => 'array',
        'Results' => 'array',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [];

    // Relation

    /**
     * @return User|Collection|Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return PaymentMethod|Collection|Relations\BelongsTo
     */
    public function payment_stripe()
    {
        return $this->belongsTo(PaymentMethod::class);
    }
}
