<?php

namespace Custom\Stripe\Results;

use Carbon\Carbon;

// サンプル
// {
//  "id": "ch_1GhXdj2eZvKYlo2CDYDSlf8z",
//  "object": "charge",
//  "amount": 100,
//  "amount_refunded": 0,
//  "application": null,
//  "application_fee": null,
//  "application_fee_amount": null,
//  "balance_transaction": "txn_19XJJ02eZvKYlo2ClwuJ1rbA",
//  "billing_details": {
//    "address": {
//      "city": null,
//      "country": null,
//      "line1": null,
//      "line2": null,
//      "postal_code": null,
//      "state": null
//    },
//    "email": null,
//    "name": null,
//    "phone": null
//  },
//  "calculated_statement_descriptor": null,
//  "captured": false,
//  "created": 1589188175,
//  "currency": "usd",
//  "customer": null,
//  "description": "My First Test Charge (created for API docs)",
//  "disputed": false,
//  "failure_code": null,
//  "failure_message": null,
//  "fraud_details": {},
//  "invoice": null,
//  "livemode": false,
//  "metadata": {},
//  "on_behalf_of": null,
//  "order": null,
//  "outcome": null,
//  "paid": true,
//  "payment_intent": null,
//  "payment_method": "card_1GhXdg2eZvKYlo2CAtaziDns",
//  "payment_method_details": {
//    "card": {
//      "brand": "visa",
//      "checks": {
//        "address_line1_check": null,
//        "address_postal_code_check": null,
//        "cvc_check": "pass"
//      },
//      "country": "US",
//      "exp_month": 8,
//      "exp_year": 2021,
//      "fingerprint": "Xt5EWLLDS7FJjR1c",
//      "funding": "credit",
//      "installments": null,
//      "last4": "4242",
//      "network": "visa",
//      "three_d_secure": null,
//      "wallet": null
//    },
//    "type": "card"
//  },
//  "receipt_email": null,
//  "receipt_number": null,
//  "receipt_url": "https://pay.stripe.com/receipts/acct_1032D82eZvKYlo2C/ch_1GhXdj2eZvKYlo2CDYDSlf8z/rcpt_HG3lhbxJB9vvqVGCudGVno08tx1WTXr",
//  "refunded": false,
//  "refunds": {
//    "object": "list",
//    "data": [],
//    "has_more": false,
//    "url": "/v1/charges/ch_1GhXdj2eZvKYlo2CDYDSlf8z/refunds"
//  },
//  "review": null,
//  "shipping": null,
//  "source_transfer": null,
//  "statement_descriptor": null,
//  "statement_descriptor_suffix": null,
//  "status": "succeeded",
//  "transfer_data": null,
//  "transfer_group": null
//}{
//  "id": "ch_1GhXdj2eZvKYlo2CDYDSlf8z",
//  "object": "charge",
//  "amount": 100,
//  "amount_refunded": 0,
//  "application": null,
//  "application_fee": null,
//  "application_fee_amount": null,
//  "balance_transaction": "txn_19XJJ02eZvKYlo2ClwuJ1rbA",
//  "billing_details": {
//    "address": {
//      "city": null,
//      "country": null,
//      "line1": null,
//      "line2": null,
//      "postal_code": null,
//      "state": null
//    },
//    "email": null,
//    "name": null,
//    "phone": null
//  },
//  "calculated_statement_descriptor": null,
//  "captured": false,
//  "created": 1589188175,
//  "currency": "usd",
//  "customer": null,
//  "description": "My First Test Charge (created for API docs)",
//  "disputed": false,
//  "failure_code": null,
//  "failure_message": null,
//  "fraud_details": {},
//  "invoice": null,
//  "livemode": false,
//  "metadata": {},
//  "on_behalf_of": null,
//  "order": null,
//  "outcome": null,
//  "paid": true,
//  "payment_intent": null,
//  "payment_method": "card_1GhXdg2eZvKYlo2CAtaziDns",
//  "payment_method_details": {
//    "card": {
//      "brand": "visa",
//      "checks": {
//        "address_line1_check": null,
//        "address_postal_code_check": null,
//        "cvc_check": "pass"
//      },
//      "country": "US",
//      "exp_month": 8,
//      "exp_year": 2021,
//      "fingerprint": "Xt5EWLLDS7FJjR1c",
//      "funding": "credit",
//      "installments": null,
//      "last4": "4242",
//      "network": "visa",
//      "three_d_secure": null,
//      "wallet": null
//    },
//    "type": "card"
//  },
//  "receipt_email": null,
//  "receipt_number": null,
//  "receipt_url": "https://pay.stripe.com/receipts/acct_1032D82eZvKYlo2C/ch_1GhXdj2eZvKYlo2CDYDSlf8z/rcpt_HG3lhbxJB9vvqVGCudGVno08tx1WTXr",
//  "refunded": false,
//  "refunds": {
//    "object": "list",
//    "data": [],
//    "has_more": false,
//    "url": "/v1/charges/ch_1GhXdj2eZvKYlo2CDYDSlf8z/refunds"
//  },
//  "review": null,
//  "shipping": null,
//  "source_transfer": null,
//  "statement_descriptor": null,
//  "statement_descriptor_suffix": null,
//  "status": "succeeded",
//  "transfer_data": null,
//  "transfer_group": null
//}

/**
 * Stripe/Charge オブジェクト.
 * https://stripe.com/docs/api/charges/object
 *
 * @package Custom\Stripe\Results
 *
 * @property string id "ch_[ユニークID]"
 * @property string object "charge"固定
 * @property int amount 請求金額(0以上の数値)
 * @property int amount_refunded 払戻金額(0以上の数値)
 * @property string|null application 請求作成時のID { CONNECT ONLY }
 * @property string|null application_fee 手数料の申請手数料 { CONNECT ONLY }
 * @property int|null application_fee_amount 手数料の申請手数料 { CONNECT ONLY }
 * @property string balance_transaction 残高に及ぼす影響を説明する残高トランザクションID
 * @property array billing_details 請求情報 { address, email, name, phone }
 * @property string|null calculated_statement_descriptor 顧客のクレジットカードと銀行の明細書に表示される既述子
 * @property boolean captured 承認されているかどうか { true:承認済, false:未承認 }
 * @property Carbon created timestamp
 * @property string currency 通貨
 * @property string|null customer 顧客ID
 * @property string description 説明
 * @property boolean disputed 請求について意義申立があったかどうか
 * @property string|null failure_code エラーコード
 * @property string|null failure_message エラーメッセージ
 * @property array fraud_details 請求に対する詐欺評価 { stripe_report, user_report }
 * @property string|null invoice 請求ID
 * @property boolean livemode ライブモード { true:ライブモード, false:テストモード }
 * @property array metadata 付加情報
 * @property string|null on_behalf_of トリガーなしの自動転送に代わって請求されたアカウント
 * @property string|null order 注文ID
 * @property array|null outcome 支払いが受諾されたかどうか、及びその理由
 * @property boolean paid 請求 or 承認されているかどうか { true:成功, false:失敗 }
 * @property string|null payment_intent 支払い意図
 * @property string payment_method 支払方法
 * @property array payment_method_details 支払方法詳細
 * @property string|null receipt_email 領収書が送信されたメールアドレス
 * @property string|null receipt_number 領収書番号
 * @property string receipt_url 領収書が記載されたURL
 * @property boolean refunded 全額返金されたかどうか(一部返金は含まれない)
 * @property array refunds 払い戻し一覧 { object, data, has_more, url }
 * @property string|null review 請求に関連づけられたレビューID
 * @property string|null shipping 配送情報 { address, carrier, name, phone, tracking_number }
 * @property string|null source_transfer 送金ID(請求が別Stripeアカウントから作成された場合のみ)
 * @property string|null statement_descriptor 請求説明
 * @property string|null statement_descriptor_suffix 請求説明(カード料金について)
 * @property string status 支払状態 { succeeded:成功, pending:保留, failed:失敗 }
 * @property string|null transfer 転送先アカウントへの転送ID
 * @property array|null transfer_data 宛先料金の一部として自動的に転送するアカウントを含むオプション { amount, destination }
 * @property string|null transfer_group トランザクションをグループとして識別する文字列
 */
class ChargeResult extends Result
{
    /**
     * コンストラクタです.
     *
     * @param array $response
     */
    public function __construct(array $response = [])
    {
        parent::__construct($response);
    }

}
