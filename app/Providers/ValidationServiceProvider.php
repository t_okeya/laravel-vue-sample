<?php

namespace App\Providers;

use App\Validators\Validator;
use Illuminate\Support\ServiceProvider;

/**
 * Class ValidationServiceProvider.
 *
 * @package App\Providers
 */
class ValidationServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Validator::resolver(function ($translator, $data, $rules, $messages, $customAttributes) {
            return new Validator($translator, $data, $rules, $messages, $customAttributes);
        });

    }
}
