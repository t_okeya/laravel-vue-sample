<?php

namespace Custom\Stripe\Api;

use Stripe\Exception\BadMethodCallException;
use Stripe\Util\DefaultLogger;

/**
 * DefaultLoggerを拡張したLoggerクラス.
 *
 * @package Custom\Stripe\Api
 */
class Logger extends DefaultLogger
{
    /**
     * @param string $message
     * @param array $context
     */
    public function error($message, array $context = [])
    {
        if (\count($context) > 0)
        {
            throw new BadMethodCallException(
                'DefaultLogger does not currently implement context. Please implement if you need it.');
        }

        if ($this->destination === null)
        {
            error_log($message, $this->messageType);
        }
        else
        {
            error_log($message, $this->messageType, $this->destination);
        }
    }
}
