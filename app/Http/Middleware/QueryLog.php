<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Closure;

/**
 * Class QueryLog.
 *
 * @package App\Http\Middleware
 */
class QueryLog
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (env('APP_DEBUG') === false)
        {
            return $next($request);
        }

        $this->readyOutputQueryLog();

        $response = $next($request);

        $this->outputQueryLog();

        return $response;
    }

    /**
     * ready output query log.
     */
    private function readyOutputQueryLog()
    {
        DB::connection(env('DB_CONNECTION'))->enableQueryLog();
    }

    /**
     * output query log.
     */
    private function outputQueryLog()
    {
        $db_name = env('DB_CONNECTION');
        $queries = DB::connection($db_name)->getQueryLog();
        if (!empty($queries))
        {
            Log::channel('sqllog')->debug('Query log, '. $db_name);
            foreach ($queries as $query)
            {
                Log::channel('sqllog')->debug($query);
            }
        }
    }
}
