<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFailedJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('failed_jobs', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('ID');
            $table->text('connection')->comment('コネクション名');
            $table->text('queue')->comment('キュー名');
            $table->longText('payload')->comment('ペイロード');
            $table->longText('exception')->comment('例外');
            $table->timestamp('failed_at')->useCurrent();
            $table->engine = "InnoDB Comment '失敗したジョブ'";
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('failed_jobs');
    }
}
