<?php

namespace App\Http\Controllers\User\Auth;

use App\Exceptions\EmailTakenException;
use App\Http\Controllers\User\Controller;
use App\Models\OAuthProvider;
use App\Models\User;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Laravel\Socialite\Two\User as STUser;
use Laravel\Socialite\Facades\Socialite;
use Tymon\JWTAuth\JWTGuard;

/**
 * Class OAuthController.
 *
 * @package App\Http\Controllers\User\Auth
 */
class OAuthController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Redirect the user to the provider authentication page.
     *
     * @param $provider
     * @return array
     */
    public function redirect($provider)
    {
        return [
            'url' => Socialite::driver($provider)->stateless()->redirect()->getTargetUrl(),
        ];
    }

    /**
     * Obtain the user information from the provider.
     *
     * @param string $provider
     * @return Application|Factory|View
     * @throws Exception
     */
    public function callback($provider)
    {
        /** @var STUser $sUser. */
        $sUser = Socialite::driver($provider)->stateless()->user();

        try
        {
            $user = DB::transaction(function () use ($provider, $sUser){
                return $this->findOrCreateUser($provider, $sUser);
            });
        }
        catch (Exception $e)
        {
            throw $e;
        }

        $token = $this->guard()->login($user);
        $this->guard()->setToken($token);

        return view('user/oauth/callback', [
            'token'      => $token,
            'token_type' => 'bearer',
            'expires_in' => $this->guard()->getPayload()->get('exp') - time(),
        ]);
    }

    /**
     * @param string $provider
     * @param STUser $sUser
     * @return User
     * @throws EmailTakenException
     */
    protected function findOrCreateUser($provider, $sUser)
    {
        /** @var OAuthProvider $oauthProvider. */
        $oauthProvider = OAuthProvider::query()->where('provider', $provider)
            ->where('provider_user_id', $sUser->getId())
            ->first();

        if ($oauthProvider) {
            $oauthProvider->update([
                'access_token'  => $sUser->token,
                'refresh_token' => $sUser->refreshToken,
            ]);

            return $oauthProvider->user;
        }

        if (User::query()->where('email', $sUser->getEmail())->exists()) {
            throw new EmailTakenException;
        }

        return $this->createUser($provider, $sUser);
    }

    /**
     * @param string $provider
     * @param STUser $sUser
     * @return User
     */
    protected function createUser($provider, $sUser)
    {
        /** @var User $user. */
        $user = User::query()->create([
            'name'              => $sUser->getName(),
            'email'             => $sUser->getEmail(),
            'email_verified_at' => now(),
            'plan_id'           => 1,
            'customer_id'       => NULL,
        ]);

        $user->oauth_providers()->create([
            'provider'         => $provider,
            'provider_user_id' => $sUser->getId(),
            'access_token'     => $sUser->token,
            'refresh_token'    => $sUser->refreshToken,
        ]);

        return $user;
    }


    // Override AuthenticatesUsers

    /**
     * Get the guard to be used during authentication.
     *
     * @return JWTGuard
     */
    protected function guard()
    {
        return Auth::guard('users');
    }
}
