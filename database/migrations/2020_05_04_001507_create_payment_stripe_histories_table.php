<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentStripeHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_stripe_histories', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('ID');
            $table->bigInteger('user_id')->unsigned()->comment('ユーザID');
            $table->bigInteger('payment_method_id')->unsigned()->comment('決済情報ID');
            $table->longText('requests')->comment('リクエスト情報');
            $table->longText('results')->comment('レスポンス情報');
            $table->timestamps();
            $table->engine = "InnoDB Comment 'Stripe決済履歴'";
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_stripe_histories');
    }
}
