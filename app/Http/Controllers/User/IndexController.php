<?php

namespace App\Http\Controllers\User;

use \Illuminate\Contracts\Foundation\Application;
use \Illuminate\Contracts\View\Factory;
use \Illuminate\View\View;

class IndexController extends Controller
{
    /**
     * Get the SPA view.
     *
     * @return Application|Factory|View
     */
    public function __invoke()
    {
        return $this->view([], 'user.index');
    }
}
