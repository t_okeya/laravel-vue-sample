<?php

namespace App\Http\Requests\User\Settings\Password;

class UpdateRequest extends BaseRequest
{
    /**
     * @return array
     */
    protected function fields()
    {
        return parent::commonFields();;
    }

    /**
     * @return array
     */
    public function messages()
    {
        return parent::commonMessages();
    }
}
