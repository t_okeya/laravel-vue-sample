<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations;


/**
 * Class OauthProvider.
 *
 * @package App\Models
 *
 * @property mixed user_id ユーザID
 * @property mixed provider プロバイダ(github, twitter, facebook, etc)
 * @property mixed provider_user_id プロバイダ管理のユーザID
 * @property mixed access_token アクセストークン
 * @property mixed refresh_token リフレッシュトークン
 * @property Carbon created_at
 * @property Carbon updated_at
 * @property User|Collection|Relations\BelongsTo user
 */
class OauthProvider extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'provider', 'provider_user_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'access_token', 'refresh_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [];

    // Relation

    /**
     * @return User|Collection|Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
