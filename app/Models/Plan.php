<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations;

/**
 * Class Plan.
 *
 * @package App\Models
 *
 * @property mixed name プラン名
 * @property mixed unit_price 単価
 * @property mixed tax_category 消費税区分(1:内税, 2:外税, 3:対象外)
 * @property Carbon created_at
 * @property Carbon updated_at
 * @property User|Collection|Relations\BelongsTo user
 */
class Plan extends Model
{

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [];

    // Relation

    /**
     * @return User|Collection|Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
