<?php

namespace Custom\Stripe\Api;

use Stripe\ApiRequestor;
use Stripe\HttpClient\CurlClient;
use Stripe\Stripe;

/**
 * Stripe API をラッピングする際の基底クラス.
 *
 * @package Custom\Stripe\Api
 */
class Base
{

    /** @var CurlClient CurlClientインスタンス. */
    protected $_client = null;
    /** @var array CurlClientインスタンス生成時のオプション. */
    protected $_options = [];
    /** @var int SSLバージョン. */
    protected $SSL_VERSION = CURL_SSLVERSION_TLSv1_2;
    /** @var int 最大リトライ回数. */
    protected $MAX_RETRY = 0;
    /** @var bool テレメトリ可否. */
    protected $ENABLE_TELEMETRY = true;

    /**
     * コンストラクタです.
     *
     * @param CurlClient|null $client
     * @param array $options
     */
    protected function __construct(CurlClient $client = null, array $options = [])
    {
        // オプション設定
        $this->set_options();
        $this->_options = array_merge($this->_options, $options);

        // CurlClient設定
        $this->_client  = is_null($client) ? new CurlClient($options) : $client;
        $this->set_client();

        // Stripe設定
        $this->set_stripe();
    }

    /**
     * CurlClientインスタンス生成時のオプションを設定します.
     */
    private function set_options()
    {
        $this->set_proxy();
        $this->set_tls();
    }

    /**
     * CurlClientに関して初期設定します.
     */
    private function set_client()
    {
        $this->_client->setTimeout(config('custom.stripe.timeout'));
        $this->_client->setConnectTimeout(config('custom.stripe.connect_timeout'));
        ApiRequestor::setHttpClient($this->_client);
    }

    /**
     * Stripeに関する値を設定します.
     */
    private function set_stripe()
    {
        Stripe::setApiKey(config('custom.stripe.secret'));
        $this->set_max_retries();
        $this->set_telemetry();
        $this->set_logger();
    }

    /**
     * プロキシを設定します.
     *   デフォルト
     *     ホスト: proxy.local
     *     ポート: −1
     */
    protected function set_proxy()
    {
        $port = config('custom.stripe.proxy_port');
        if (-1 < $port) {
            $host  = config('custom.stripe.proxy_host');
            $proxy = $host . ':' . $port;
            $this->_options[CURLOPT_PROXY] = $proxy;
        }
    }

    /**
     * SSLバージョンを設定します.
     *   デフォルト: TLS 1.2
     */
    protected function set_tls()
    {
        $this->_options[CURLOPT_SSLVERSION] = $this->SSL_VERSION;
    }

    /**
     * 最大リトライ回数を設定します.
     *   デフォルト: 0
     */
    protected function set_max_retries()
    {
        Stripe::setMaxNetworkRetries($this->MAX_RETRY);
    }

    /**
     * テレメトリを設定します.
     *   デフォルト: true
     */
    protected function set_telemetry()
    {
        Stripe::setEnableTelemetry($this->ENABLE_TELEMETRY);
    }

    /**
     * Loggerを設定します.
     *   デフォルト: Stripe\Util\DefaultLoggerを拡張したLogger
     */
    protected function set_logger()
    {
        Stripe::setLogger(new Logger());
    }

}
