<?php


namespace App\Console\Commands;


use Illuminate\Foundation\Console\ModelMakeCommand;

/**
 * php artisan make:model 実行時に\App\Models\配下にファイルを作成するためのクラス.
 *
 * @package App\Console\Commands
 */
class MakeModel extends ModelMakeCommand
{
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\Models';
    }
}
