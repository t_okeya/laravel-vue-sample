<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use App\Models;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Artisan::call("migrate:refresh");

        // マスタデータ
        $this->createTaxes();
        $this->createPlans();

        if (DB::connection()->getDriverName() === 'mysql') {
            DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        }

        Model::unguard();

        // $this->call(UsersTableSeeder::class);

        Model::reguard();

        if (DB::connection()->getDriverName() === 'mysql') {
            DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        }
    }

    private function createTaxes()
    {
        $values = [
            [
                'rate'       => 0.10,
                'start_date' => '2019-10-01',
                'end_date'   => '9999-12-31',
            ]
        ];

        foreach ($values as $key => $value)
        {
            $this->createTax($value);
        }
    }

    private function createPlans()
    {
        $values = [
            [
                'name'         => '無料会員',
                'unit_price'   => 0, // 0円
                'tax_category' => 2, // 外税
            ],
            [
                'name'         => '有料会員',
                'unit_price'   => 500, // 500円
                'tax_category' => 2,   // 外税
            ]
        ];

        foreach ($values as $key => $value)
        {
            $this->createPlan($value);
        }
    }

    /**
     * @param array $data
     * @return Collection|Model|Models\Plan
     */
    protected function createPlan(array $data = array())
    {
        return factory(App\Models\Plan::class, 1)->create($data);
    }

    /**
     * @param array $data
     * @return Collection|Model|Models\Tax
     */
    protected function createTax(array $data = array())
    {
        return factory(App\Models\Tax::class, 1)->create($data);
    }
}
