import axios from 'axios'
import Cookies from 'js-cookie'
import * as types from '../mutation-types'

// state
export const state = {
  user : null, // ユーザ情報
  cards: null, // 決済情報
  token: Cookies.get('token'), // トークン
}

// getters
export const getters = {
  user: state => state.user,
  token: state => state.token,
  cards: state => state.cards,
  check: state => state.user !== null
}

// mutations
export const mutations = {

  [types.SAVE_TOKEN] (state, { token, remember }) {
    state.token = token
    Cookies.set('token', token, { expires: remember ? 365 : null })
  },

  [types.FETCH_USER_SUCCESS] (state, { user }) {
    state.user = user
  },

  [types.FETCH_USER_FAILURE] (state) {
    state.token = null
    Cookies.remove('token')
  },

  [types.LOGOUT] (state) {
    state.user = null
    state.token = null
    Cookies.remove('token')
  },

  [types.UPDATE_USER] (state, { user }) {
    state.user = user
  },

  [types.FETCH_CARDS_SUCCESS] (state, { cards }) {
    state.cards = cards
  },

  [types.FETCH_CARDS_FAILURE] (state) {
    state.cards = null
  },

  [types.UPDATE_CARDS] (state, { cards }) {
    state.cards = cards
  },

  [types.DELETE_CARD] (state) {
    state.card = null
  }
}

// actions
export const actions = {

  saveToken ({ commit, dispatch }, payload) {
    commit(types.SAVE_TOKEN, payload)
  },

  async fetchUser ({ commit }) {
    try {
      const { data } = await axios.get('/api/me')
      commit(types.FETCH_USER_SUCCESS, { user: data })
    } catch (e) {
      commit(types.FETCH_USER_FAILURE)
    }
  },

  updateUser ({ commit }, payload) {
    commit(types.UPDATE_USER, payload)
  },

  async logout ({ commit }) {
    try {
      await axios.post('/api/logout')
    } catch (e) {

    }
    commit(types.LOGOUT)
  },

  async fetchOauthUrl (ctx, { provider }) {
    const { data } = await axios.post(`/api/oauth/${provider}`)
    return data.url
  },

  async fetchCards ({ commit }) {
    try {
      // const { data } = await axios.get('/api/settings/default')
      const { data } = await axios.get('/api/settings/payments')
      commit(types.FETCH_CARDS_SUCCESS, { cards: data })
    } catch (e) {
      commit(types.FETCH_CARDS_FAILURE)
    }
  },

  updateCards ({ commit }, payload) {
    commit(types.UPDATE_CARDS, payload)
  },

  async deleteCard({ commit }, payload) {
    await axios.delete('/api/settings/payment', { 'data': payload })
    commit(types.DELETE_CARD)
  }
}
