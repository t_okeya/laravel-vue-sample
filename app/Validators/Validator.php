<?php

namespace App\Validators;

use Illuminate\Contracts\Translation\Translator;
use Illuminate\Validation\Validator as BaseValidator;

/**
 * Class Validator.
 *
 * @package App\Validators
 */
class Validator extends BaseValidator
{
    /** パスワードで使える記号列挙 */
    const PASSWORD_SYMBOLS = '!"#$%&\'()*+,-./:;<=>?@[]^_`{|}~';

    /**
     * Validator constructor.
     *
     * @param Translator $translator
     * @param array $data
     * @param array $rules
     * @param array $messages
     * @param array $customAttributes
     */
    public function __construct(Translator $translator, array $data, array $rules,
                                array $messages = [], array $customAttributes = [])
    {
        parent::__construct($translator, $data, $rules, $messages, $customAttributes);

        $this->implicitRules[] = 'RequiredSelect';
    }

    protected function validateRequiredSelect($attribute, $value)
    {
        return $this->validateRequired($attribute, $value);
    }

    protected function validatePassword($attribute, $value)
    {
        $symbols = preg_quote(self::PASSWORD_SYMBOLS, '/');
        $regex = "/^[a-zA-Z0-9{$symbols}]*$/";
        return $this->validateRegex($attribute, $value, [$regex]);
    }

    protected function validateMixCase($attribute, $value)
    {
        $regex = '/([a-z].*[A-Z]|[A-Z].*[a-z])/';
        return $this->validateRegex($attribute, $value, [$regex]);
    }

    protected function validateMixAlnum($attribute, $value)
    {
        $regex = '/([a-z].*\d|\d.*[a-z])/i';
        return $this->validateRegex($attribute, $value, [$regex]);
    }

    protected function validateHankaku($attribute, $value)
    {
        ($attribute);
        return (bool)preg_match("/^[\x20-\x7E]+$/", $value);
    }

    protected function validateZenkaku($attribute, $value)
    {
        ($attribute);
        return (bool)preg_match('/^[ぁ-んァ-ヶー一-龠]+$/u', $value);
    }

    protected function validateZenkakuKana($attribute, $value)
    {
        ($attribute);
        return (bool)preg_match('/^[ア-ン゛゜ァ-ォャ-ョー「」、]+$/u', $value);
    }

    protected function validateZipCode($attribute, $value)
    {
        ($attribute);
        if (preg_match("/^\d{3}-\d{4}$/", $value)){
            return true;
        } else {
            return (bool)preg_match("/^\d{7}$/", $value);
        }
    }

    protected function validatePhoneNumber($attribute, $value)
    {
        ($attribute);
        if (preg_match("/^[0-9]{2,4}-[0-9]{2,4}-[0-9]{3,4}$/", $value)){
            return true;
        } else {
            return (bool)preg_match("/^\d{0,7}$/", $value);
        }
    }

    protected function validateMobileNumber($attribute, $value)
    {
        ($attribute);
        if (preg_match("/^[0-9]{2,4}-[0-9]{2,4}-[0-9]{3,4}$/", $value)){
            return true;
        } else {
            return (bool)preg_match("/^\d{0,7}$/", $value);
        }
    }

}
