<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations;

/**
 * Class PaymentMethod.
 *
 * @package App\Models
 *
 * @property mixed user_id ユーザID
 * @property mixed customer_id 顧客ID(Stripe管理の顧客ID)
 * @property mixed card_id カードID(Stripe管理のカードID)
 * @property boolean default_used デフォルトで使用するかどうか
 * @property Carbon created_at
 * @property Carbon updated_at
 * @property Carbon deleted_at
 * @property User|Collection|Relations\BelongsTo user
 * @property PaymentStripeHistory[]|Collection|Relations\HasMany payment_stripe_histories
 */
class PaymentMethod extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'customer_id', 'card_id', 'default_used'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [];

    // Relation

    /**
     * @return User|Collection|Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return PaymentStripeHistory[]|Collection|Relations\HasMany
     */
    public function payment_stripe_histories()
    {
        return $this->hasMany(PaymentStripeHistory::class);
    }
}
