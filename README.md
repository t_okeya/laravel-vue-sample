# Laravel + Vue.js

## initialize

```sh
# prepare
$ which composer || brew install homebrew/php/composer

# node module install
$ npm i

# php module install
$ composer install

# laravel initialize
$ cat .env.local > .env
$ docker exec -it laravel-vue_web_1 php artisan key:generate

# container start
$ npm run up

# create database
$ docker exec -it laravel-vue_db_1 mysql -uroot -e "create database sample_db default character set utf8mb4 collate 'utf8mb4_unicode_ci'"

# migrate
$ docker exec -it laravel-vue_web_1 php artisan migrate
$ docker exec -it laravel-vue_web_1 php artisan db:seed

# front resource build
$ npm run watch

# container stop
$ npm run down
```

## etc

```sh
# Seeder個別実行方法
$ docker exec -it laravel-vue_web_1 php artisan db:seed --class=DatabaseSeeder
```

```sh
$ docker exec -it laravel-vue_web_1 php artisan tinker
$ factory(App\Models\User::class, 2)->create();
```

```sh
$ composer dumpautoload -o
```

```ssh
$ docker exec -it laravel-vue_db_1 mysql -uroot -e "drop database sample_db"
```

## laravel-debugar

```sh
# laravel-debugar install
$ composer require barryvdh/laravel-debugbar --dev
$ php artisan vendor:publish --provider="Barryvdh\Debugbar\ServiceProvider"

# how to
## modified .env file
DEBUGBAR_ENABLED=true

## example
$user = $this->guard()->user();
\Debugbar::info($users);
```

## clockwork
```sh
# chrome debugar 
https://chrome.google.com/webstore/detail/clockwork/dmggabnehkmmfmdffgajcflpdjlnoemp

# clockwork install
$ composer require --dev itsgoingd/clockwork

# how to
## modified .env file
DEBUGBAR_ENABLED=true

## example

## access url ↓↓
http://localhost:8000/__clockwork/app#
```

## fontawesome
- [fontawesome icons](https://fontawesome.com/icons?d=gallery).

## stripe test card number
- [stripe test card](https://stripe.com/docs/testing#cards)

## memo

```sh
# create laravel project
composer create-project --prefer-dist laravel/laravel laravel-vue-sample "6.*"
```

```sh
# install laravel/ui
composer require laravel/ui
```

```sh
# basic scafold
docker exec -it laravel-vue_web_1 php artisan ui vue
```

```sh
# auth scafold
docker exec -it laravel-vue_web_1 php artisan ui vue --auth
```

```sh
# App\Providers\AppServiceProviderに以下追加

```sh
use Illuminate\Support\Facades\Schema;
~
~
public function boot()
{
    // Fix for MySQL < 5.7.7 and MariaDB < 10.2.2
    // https://laravel.com/docs/master/migrations#creating-indexes
    Schema::defaultStringLength(191);
}
```


<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains over 1500 video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for funding Laravel development. If you are interested in becoming a sponsor, please visit the Laravel [Patreon page](https://patreon.com/taylorotwell).

- **[Vehikl](https://vehikl.com/)**
- **[Tighten Co.](https://tighten.co)**
- **[Kirschbaum Development Group](https://kirschbaumdevelopment.com)**
- **[64 Robots](https://64robots.com)**
- **[Cubet Techno Labs](https://cubettech.com)**
- **[Cyber-Duck](https://cyber-duck.co.uk)**
- **[British Software Development](https://www.britishsoftware.co)**
- **[Webdock, Fast VPS Hosting](https://www.webdock.io/en)**
- **[DevSquad](https://devsquad.com)**
- [UserInsights](https://userinsights.com)
- [Fragrantica](https://www.fragrantica.com)
- [SOFTonSOFA](https://softonsofa.com/)
- [User10](https://user10.com)
- [Soumettre.fr](https://soumettre.fr/)
- [CodeBrisk](https://codebrisk.com)
- [1Forge](https://1forge.com)
- [TECPRESSO](https://tecpresso.co.jp/)
- [Runtime Converter](http://runtimeconverter.com/)
- [WebL'Agence](https://weblagence.com/)
- [Invoice Ninja](https://www.invoiceninja.com)
- [iMi digital](https://www.imi-digital.de/)
- [Earthlink](https://www.earthlink.ro/)
- [Steadfast Collective](https://steadfastcollective.com/)
- [We Are The Robots Inc.](https://watr.mx/)
- [Understand.io](https://www.understand.io/)
- [Abdel Elrafa](https://abdelelrafa.com)
- [Hyper Host](https://hyper.host)
- [Appoly](https://www.appoly.co.uk)
- [OP.GG](https://op.gg)

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Code of Conduct

In order to ensure that the Laravel community is welcoming to all, please review and abide by the [Code of Conduct](https://laravel.com/docs/contributions#code-of-conduct).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
