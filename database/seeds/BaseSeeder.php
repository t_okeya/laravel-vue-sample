<?php

use Illuminate\Database\Eloquent;
use Illuminate\Database\Seeder;
use App\Models;

class BaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
    }

    /**
     * Serviceを利用する場合に利用します.
     *   $app->make(Service::class);
     *
     * @return mixed
     */
    protected function createApplication()
    {
        $app = require __DIR__.'/../../bootstrap/app.php';
        $app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();
        return $app;
    }

    /**
     * @param array $data
     * @return Eloquent\Collection|Eloquent\Model|Models\Plan
     */
    protected function createPlan(array $data = array())
    {
        return factory(App\Models\Plan::class, 1)->create($data);
    }

    /**
     * @param array $data
     * @return Eloquent\Collection|Eloquent\Model|Models\Tax
     */
    protected function createTax(array $data = array())
    {
        return factory(App\Models\Tax::class, 1)->create($data);
    }
}
