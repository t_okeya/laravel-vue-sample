<?php

namespace Custom\Stripe;

use Illuminate\Support\Manager;
use Stripe\HttpClient\CurlClient;

/**
 * マネージャ.
 *
 * @package Custom\Stripe
 */
class StripeManager extends Manager
{

    /**
     * @return mixed
     */
    protected function createCustomerDriver()
    {
        return $this->buildProvider('Custom\Stripe\Api\Customer');
    }

    /**
     * @return mixed
     */
    protected function createChargeDriver()
    {
        return $this->buildProvider('Custom\Stripe\Api\Charge');
    }

    /**
     * @param $provider
     * @param CurlClient|null $client
     * @param array $options
     * @return mixed
     */
    public function buildProvider($provider, CurlClient $client = null, array $options = [])
    {
        return new $provider($client, $options);
    }

    /**
     * Get the default driver name.
     *
     * @return string
     */
    public function getDefaultDriver()
    {
        throw new \InvalidArgumentException('No Custom Stripe API driver was specified.');
    }
}
