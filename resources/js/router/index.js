/**
 * vue-routerを使用する上で共通処理を使いまわせるように
 * 切り出したもの(middleware)をvue-routerのライフサイクルに組み込みます.
 **/

import Vue from 'vue'
import store from '~/store'
import Meta from 'vue-meta'
import routes from './routes'
import Router from 'vue-router'
import { sync } from 'vuex-router-sync'

Vue.use(Meta)
Vue.use(Router)

// 全画面で利用可能なミドルウェア.
const globalMiddleware = ['locale', 'check-auth']

// middleware/直下のファイルを全て拾う.
const routeMiddleware = resolveMiddleware(
  require.context('~/middleware', false, /.*\.js$/)
)

// Vue-Routerインスタンス生成及びフック登録
const router = createRouter()

// Vuexのstoreで、route情報にアクセス可能にする.
sync(store, router)

export default router


// method

/**
 * Vue-Routerのインスタンスを生成及び、フックの登録を行います.
 *
 * @return {VueRouter}
 */
function createRouter () {
  const router = new Router({
    scrollBehavior,
    mode: 'history',
    routes
  })

  router.beforeEach(beforeEach) // beforeガードの登録
  router.afterEach(afterEach)   // afterガードの登録

  return router
}

/**
 * Guard処理を行います(前処理).
 * ルーターのライフサイクルの中で呼ばれるメソッド.
 *
 * @param {Route} to
 * @param {Route} from
 * @param {Function} next
 */
async function beforeEach (to, from, next) {
  let components = []

  try {
    // 現在のルートまたは提供されたロケーションにマッチしているコンポーネントの配列を返却
    components = await resolveComponents(router.getMatchedComponents({ ...to }))
  } catch (error) {
    if (/^Loading( CSS)? chunk (\d)+ failed\./.test(error.message)) {
      window.location.reload(true)
      return
    }
  }

  if (components.length === 0) {
    return next()
  }

  // ローディング開始
  if (components[components.length - 1].loading !== false) {
    router.app.$nextTick(() => router.app.$loading.start())
  }

  // 一致するすべてのコンポーネントのミドルウェアを取得.
  const middleware = getMiddleware(components)

  // 各ミドルウェアを呼び出す.
  callMiddleware(middleware, to, from, (...args) => {
    if (args.length === 0) {
      // 引数なしで「next()」が呼び出された場合、アプリケーションレイアウトを設定する
      router.app.setLayout(components[0].layout || '')
    }

    next(...args)
  })
}

/**
 * フック処理を行います(後処理).
 * ルーターのライフサイクルの中で呼ばれるメソッド.
 *
 * @param {Route} to
 * @param {Route} from
 * @param {Function} next
 */
async function afterEach (to, from, next) {
  await router.app.$nextTick()

  router.app.$loading.finish()
}

/**
 * 各ミドルウェアを呼び出します.
 *   ミドルウェアは複数指定が可能なため、ミドルウェアのスタックを再帰的に処理.
 *   各ミドルウェアは _next をコールして解決する.
 *   middleware が function の場合は、その関数をそのまま実行する.
 *   middleware が string の場合は routeMiddleware 配列から既定のミドルウェアを実行する.
 *
 * @param {Array} middleware
 * @param {Route} to
 * @param {Route} from
 * @param {Function} next
 */
function callMiddleware (middleware, to, from, next) {
  const stack = middleware.reverse()

  const _next = (...args) => {
    if (args.length > 0 || stack.length === 0) {
      if (args.length > 0) {
        router.app.$loading.finish()
      }
      return next(...args)
    }

    const middleware = stack.pop()

    if (typeof middleware === 'function') {
      middleware(to, from, _next)
    } else if (routeMiddleware[middleware]) {
      routeMiddleware[middleware](to, from, _next)
    } else {
      throw Error(`Undefined middleware [${middleware}]`)
    }
  }

  _next()
}

/**
 * 非同期コンポーネントを解決します.
 *
 * @param  {Array} components
 * @return {Array}
 */
function resolveComponents (components) {
  return Promise.all(components.map(component => {
    return typeof component === 'function' ? component() : component
  }))
}

/**
 * 以下をマージします.
 *   global middleware.
 *   components middleware.
 *
 * @param  {Array} components
 * @return {Array}
 */
function getMiddleware (components) {
  const middleware = [...globalMiddleware]

  components.filter(c => c.middleware).forEach(component => {
    if (Array.isArray(component.middleware)) {
      middleware.push(...component.middleware)
    } else {
      middleware.push(component.middleware)
    }
  })

  return middleware
}

/**
 * Scroll Behavior
 *
 * @link https://router.vuejs.org/en/advanced/scroll-behavior.html
 *
 * @param  {Route} to
 * @param  {Route} from
 * @param  {Object|undefined} savedPosition
 * @return {Object}
 */
function scrollBehavior (to, from, savedPosition) {
  if (savedPosition) {
    return savedPosition
  }

  if (to.hash) {
    return { selector: to.hash }
  }

  const [component] = router.getMatchedComponents({ ...to }).slice(-1)

  if (component && component.scrollToTop === false) {
    return {}
  }

  return { x: 0, y: 0 }
}

/**
 * @param  {Object} requireContext
 * @return {Object}
 */
function resolveMiddleware (requireContext) {
  return requireContext.keys()
    .map(file =>
      [file.replace(/(^.\/)|(\.js$)/g, ''), requireContext(file)]
    )
    .reduce((guards, [name, guard]) => (
      { ...guards, [name]: guard.default }
    ), {})
}
