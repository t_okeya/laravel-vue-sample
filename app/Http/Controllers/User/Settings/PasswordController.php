<?php

namespace App\Http\Controllers\User\Settings;

use App\Http\Controllers\User\Controller;
use App\Http\Requests\User\Settings\Password\UpdateRequest;

/**
 * Class PasswordController.
 *
 * @package App\Http\Controllers\User\Settings
 */
class PasswordController extends Controller
{
    /**
     * Update the user's password.
     *
     * @param UpdateRequest $request
     */
    public function update(UpdateRequest $request)
    {
        $values = $request->values();

        $values['password'] = bcrypt($values['password']);

        $request->user()->update($values);
    }
}
