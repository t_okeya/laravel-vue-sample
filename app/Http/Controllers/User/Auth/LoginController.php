<?php

namespace App\Http\Controllers\User\Auth;

use App\Events\Auth\UserLoggedIn;
use App\Events\Auth\UserLoggedOut;
use App\Exceptions\VerifyEmailException;
use App\Http\Controllers\User\Controller;
use App\Models\User;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Contracts\Auth\StatefulGuard;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Tymon\JWTAuth\JWT;
use Tymon\JWTAuth\JWTGuard;

/**
 * Class LoginController.
 *
 * @package App\Http\Controllers\User\Auth
 */
class LoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:users')->except('logout');
    }


    // Override AuthenticatesUsers

    /**
     * Handle a login request to the application.
     *
     * @param Request $request
     * @return JsonResponse|void
     * @throws ValidationException
     * @throws VerifyEmailException
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);

        // ThrottlesLogins traitを使用している場合、ログイン試行を自動的に調整できます.
        // これらのリクエストを行うクライアントのユーザ名とIPアドレスでキーイングします.
        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // ログインに失敗した場合は、ログイン試行回数を増やし、ログインフォームにリダイレクトします.
        // もちろん、このユーザが最大試行回数を超えると、ロックアウトされます.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * Validate the user login request.
     *
     * @param Request $request
     *
     * throws ValidationException
     */
    protected function validateLogin(Request $request)
    {
        $request->validate([
            $this->username() => 'required|string',
            'password' => 'required|string',
        ]);
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param Request $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        $token = $this->guard()->attempt($this->credentials($request));

        if (!$token) {
            return false;
        }

        $user = $this->guard()->user();
        if ($user instanceof MustVerifyEmail && !$user->hasVerifiedEmail()) {
            return false;
        }

        /** @var JWTGuard $guard. */
        $guard = $this->guard();
        $guard->setToken($token);

        return true;
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param Request $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return $request->only($this->username(), 'password');
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param Request $request
     * @return JsonResponse
     */
    protected function sendLoginResponse(Request $request)
    {
        $this->clearLoginAttempts($request);

        /** @var JWT $guard. */
        $guard = $this->guard();
        $token = (string) $guard->getToken();
        $expiration = $this->guard()->getPayload()->get('exp');

        /** @var User $user. */
        $user = $this->guard()->user();
        event(new UserLoggedIn($user));

        return response()->json([
            'token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $expiration - time(),
        ]);
    }

    /**
     * Get the failed login response instance.
     *
     * @param Request $request
     * @throws ValidationException
     * @throws VerifyEmailException
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        ($request);

        $user = $this->guard()->user();
        if ($user instanceof MustVerifyEmail && ! $user->hasVerifiedEmail()) {
            /** @var User $user. */
            throw VerifyEmailException::forUser($user);
        }

        throw ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
        ]);
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'email';
    }

    /**
     * Log the user out of the application.
     *
     * @param Request $request
     */
    public function logout(Request $request)
    {
        ($request);

        /** @var User $user. */
        $user = $this->guard()->user();
        event(new UserLoggedOut($user));
        $this->guard()->logout();
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return StatefulGuard|JWT|JWTGuard
     */
    protected function guard()
    {
        return Auth::guard('users');
    }
}
