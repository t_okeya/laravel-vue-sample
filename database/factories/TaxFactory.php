<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Tax;
use Faker\Generator as Faker;

/**
 * factory(App\Models\Tax::class, 1)->create()
 */
$factory->define(Tax::class, function (Faker $faker) {
    ($faker);
    return [
        'rate'       => 0.10,
        'start_date' => '2019-10-01',
        'end_date'   => '9999-12-31',
    ];
});
