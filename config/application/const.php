<?php

return [
    'list' => [
        // 一覧画面の最大表示件数
        'max'      => 20,
        // 件数上限なし
        'no_limit' => -1,
    ],
];
