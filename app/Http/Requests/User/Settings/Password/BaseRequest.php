<?php

namespace App\Http\Requests\User\Settings\Password;

use App\Http\Requests\Request;

class BaseRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * 共通フィールド.
     *
     * @param array $addFields
     * @return array
     */
    protected function commonFields($addFields = [])
    {
        return array_merge([
            $this->field('password', '新しいパスワード', 'required|confirmed|min:6'),
            $this->field('password_confirmation', '確認パスワード', 'required|min:6'),
        ], $addFields);
    }

    /**
     * 共通メッセージ.
     *
     * @param array $addMessages
     * @return array
     */
    protected function commonMessages($addMessages = [])
    {
        return array_merge([

        ], $addMessages);
    }
}
