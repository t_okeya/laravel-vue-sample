<?php

namespace App\Listeners\Auth;

use App\Events\Auth\UserLoggedIn;
use App\Events\Auth\UserLoggedOut;
use App\Listeners\EventSubscriber;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Events\Dispatcher;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

/**
 * Class UserEventSubscriber.
 *
 * @package App\Listeners\Auth
 */
class UserEventSubscriber extends EventSubscriber
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        parent::handle($event);
    }

    /**
     * @param UserLoggedIn $event
     */
    public function onUserLogin(UserLoggedIn $event)
    {
        Log::info("user id : {$event->user->id}. login start");
    }

    /**
     * @param UserLoggedOut $event
     */
    public function onUserLogout(UserLoggedOut $event)
    {
        Log::info("user id : {$event->user->id}. login end");
    }

    /**
     * @param Dispatcher $events
     */
    public function subscribe(Dispatcher $events)
    {
        $events->listen(
            'App\Events\Auth\UserLoggedIn',
            'App\Listeners\Auth\UserEventSubscriber@onUserLogin'
        );

        $events->listen(
            'App\Events\Auth\UserLoggedOut',
            'App\Listeners\Auth\UserEventSubscriber@onUserLogout'
        );
    }
}
