<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentMethodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_methods', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('ID');
            $table->bigInteger('user_id')->unsigned()->comment('ユーザID');
            $table->string('customer_id')->index()->comment('Stripe管理の顧客ID');
            $table->string('card_id')->index()->comment('Stripe管理のカードID');
            $table->boolean('default_used')->comment('デフォルトで使用するかどうか');
            $table->timestamps();
            $table->engine = "InnoDB Comment 'Stripe情報'";

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_methods');
    }
}
