<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('ID');
            $table->string('name')->comment('氏名');
            $table->string('email')->unique()->comment('メールアドレス');
            $table->timestamp('email_verified_at')->nullable()->comment('メール承認日');
            $table->string('password')->nullable()->comment('パスワード');
            $table->rememberToken()->comment('継続ログイントークン');
            $table->string('customer_id')->nullable()->comment('Stripe管理の顧客ID');
            $table->bigInteger('plan_id')->unsigned()->comment('プランID');
            $table->timestamps();
            $table->softDeletes();
            $table->engine = "InnoDB Comment 'ユーザ情報'";
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
