<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\PaymentMethod;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/**
 * factory(App\Models\PaymentStripe::class, 1)->create()
 */
$factory->define(PaymentMethod::class, function (Faker $faker) {
    return [
        'user_id'      => factory(App\Models\User::class)->create()->get('id'),
        'customer_id'  => 'cus_' . Str::random(14),
        'card_id'      => 'card_' . Str::random(24),
        'default_used' => true
    ];
});
