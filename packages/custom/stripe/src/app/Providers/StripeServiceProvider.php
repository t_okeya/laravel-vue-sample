<?php

namespace Custom\Stripe\Providers;

use Custom\Stripe\StripeManager;
use Illuminate\Support\ServiceProvider;

/**
 * プロバイダ.
 *
 * @package Custom\Stripe\Providers
 */
class StripeServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes(array(
            __DIR__ . '/../../config/custom/stripe.php' => config_path('custom/stripe.php'),
        ));
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('stripe', function ($app) {
            return new StripeManager($app);
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['stripe'];
    }
}
