import Vue from 'vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

// import { } from '@fortawesome/free-regular-svg-icons'

import {
  faUser, faLock, faSignOutAlt, faCog, faCreditCard, faPlus, faMinus
} from '@fortawesome/free-solid-svg-icons'

import {
  faGithub, faBitbucket,
  faCcVisa, faCcMastercard, faCcJcb, faCcAmex, faCcDinersClub
} from '@fortawesome/free-brands-svg-icons'

library.add(
  faUser, faLock, faSignOutAlt, faCog, faCreditCard, faPlus, faMinus,
  faGithub, faBitbucket,
  faCcVisa, faCcMastercard, faCcJcb, faCcAmex, faCcDinersClub
)

Vue.component('fa', FontAwesomeIcon)
