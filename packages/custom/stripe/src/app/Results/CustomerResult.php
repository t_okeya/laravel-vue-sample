<?php

namespace Custom\Stripe\Results;

use Carbon\Carbon;

// サンプル
// {
//  "id": "cus_HEdriP4I83Ylv2",
//  "object": "customer",
//  "address": null,
//  "balance": 0,
//  "created": 1588861181,
//  "currency": "jpy",
//  "default_source": null,
//  "delinquent": false,
//  "description": null,
//  "discount": null,
//  "email": null,
//  "invoice_prefix": "0B56331",
//  "invoice_settings": {
//    "custom_fields": null,
//    "default_payment_method": null,
//    "footer": null
//  },
//  "livemode": false,
//  "metadata": {},
//  "name": null,
//  "next_invoice_sequence": 1,
//  "phone": null,
//  "preferred_locales": [],
//  "shipping": null,
//  "sources": {
//    "object": "list",
//    "data": [],
//    "has_more": false,
//    "url": "/v1/customers/cus_HEdriP4I83Ylv2/sources"
//  },
//  "subscriptions": {
//    "object": "list",
//    "data": [],
//    "has_more": false,
//    "url": "/v1/customers/cus_HEdriP4I83Ylv2/subscriptions"
//  },
//  "tax_exempt": "none",
//  "tax_ids": {
//    "object": "list",
//    "data": [],
//    "has_more": false,
//    "url": "/v1/customers/cus_HEdriP4I83Ylv2/tax_ids"
//  }
//}{
//  "id": "cus_HEdriP4I83Ylv2",
//  "object": "customer",
//  "address": null,
//  "balance": 0,
//  "created": 1588861181,
//  "currency": "jpy",
//  "default_source": null,
//  "delinquent": false,
//  "description": null,
//  "discount": null,
//  "email": null,
//  "invoice_prefix": "0B56331",
//  "invoice_settings": {
//    "custom_fields": null,
//    "default_payment_method": null,
//    "footer": null
//  },
//  "livemode": false,
//  "metadata": {},
//  "name": null,
//  "next_invoice_sequence": 1,
//  "phone": null,
//  "preferred_locales": [],
//  "shipping": null,
//  "sources": {
//    "object": "list",
//    "data": [],
//    "has_more": false,
//    "url": "/v1/customers/cus_HEdriP4I83Ylv2/sources"
//  },
//  "subscriptions": {
//    "object": "list",
//    "data": [],
//    "has_more": false,
//    "url": "/v1/customers/cus_HEdriP4I83Ylv2/subscriptions"
//  },
//  "tax_exempt": "none",
//  "tax_ids": {
//    "object": "list",
//    "data": [],
//    "has_more": false,
//    "url": "/v1/customers/cus_HEdriP4I83Ylv2/tax_ids"
//  }
//}

/**
 * Stripe/Customer オブジェクト.
 * https://stripe.com/docs/api/customers/object
 *
 * @package Custom\Stripe\Results
 *
 * @property string id "cus_[ユニークID]"
 * @property string object "customer"固定
 * @property array|null address 住所用オブジェクト
 * @property int balance 経常収支
 * @property Carbon created timestamp
 * @property string currency 通貨
 * @property string|null default_source デフォルト決済情報
 * @property boolean delinquent 延滞可否フラグ{ true:延滞あり, false:延滞なし }
 * @property string|null description 説明
 * @property array|null discount 割引用オブジェクト
 * @property string|null email 顧客メールアドレス
 * @property string invoice_prefix 請求番号のプレフィックス
 * @property array|null invoice_settings デフォルト請求設定 { custom_fields, default_payment_method, footer }
 * @property boolean livemode ライブモード { true:ライブモード, false:テストモード }
 * @property array metadata 付加情報 key-valueの構造体
 * @property string|null name 顧客名
 * @property int next_invoice_sequence 次回請求番号サフィックス
 * @property string|null phone 顧客電話番号
 * @property array preferred_locales 優先順に並べられた顧客使用言語
 * @property array|null shipping メール送信時に表示される顧客郵送先住所
 * @property array sources 決済情報 { object, data, has_more, url }
 * @property array subscriptions サブスクリプション情報 { object, data, has_more, url }
 * @property string tax_exempt 非課税ステータス { 'none':なし, 'exempt':免除, 'reverse':先方払い } reverseの場合、請求書と領収書に[Reverse charge]と記載される
 * @property array tax_ids 税情報 { object, data, has_more, url }
 * @property boolean deleted 削除されたかどうか(削除API実施時のみ)
 */
class CustomerResult extends Result
{

    /**
     * コンストラクタです.
     *
     * @param array $response
     */
    public function __construct(array $response = [])
    {
        parent::__construct($response);
    }

}
