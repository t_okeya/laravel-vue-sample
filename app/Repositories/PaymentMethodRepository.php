<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent;
use Illuminate\Database\Query\Builder;

/**
 * PaymentMethodRepository.
 *
 * @package App\Repositories
 */
class PaymentMethodRepository extends Repository implements Contracts\PaymentMethodRepository
{
    /** @var string $default_sort_query デフォルトソート. */
    protected $default_sort_query = 'payment_stripes.id';

    /**
     * @param $condition
     * @param null $query
     * @return Eloquent\Builder
     */
    protected function applyCondition($condition, $query = null)
    {
        $query = parent::applyCondition($condition, $query);

        foreach ($condition as $key => $value)
        {
            switch ($key)
            {
                case 'user_id':
                    if (!is_blank($value))
                    {
                        $value = like_escape($value);
                        $query->where('user_id', 'LIKE', "%$value%");
                    }
                    break;
                case 'customer_id':
                    if (!is_blank($value))
                    {
                        $query->where(function (Builder $query) use ($value) {
                            $value = like_escape($value);
                            $query->where('customer_id', 'LIKE', "%$value%");
                        });
                    }
                    break;
            }
        }
        return $query;
    }

}
